# Settings

## Installation

```
composer require machbarmacher/settings
# You must be in composer root.
vendor/bin/settings-install

```

## Prepare fsb deploy

### Create deployment credentials

* (You can instead re-use other sites' keys, but this is not recommended.)
* On an arbitrary computer, do `ssh-keygen` to a **temporary** dir
* (The pubkey will go to fsb site dashboard)
* (The private key will go to ci/cd settings) 

### Generate fsb site:

* Generate in dashboard, set primary url and Deployment-environment = live 
* Optionally add additional (redirect) domains
* Create database
* Add ssh pubkeys (for developers and deployment, see below)

### Go to CI/CD-Settings/Environment-Variables and insert secrets:

```
SSH_PRIVATE_KEY - ...
SSH_PUBKEYS - ...
SSH_URL_LIVE - sXXXX@c145s.freistilbox.net
SSH_URL_TEST - sXXXX@c145s.freistilbox.net
REPO_URL_LIVE - ssh://sXXXX@repo.freistilbox.net/~/site
REPO_URL_TEST - ssh://sXXXX@repo.freistilbox.net/~/site
SITE_URL_LIVE - https://www.example.com
SITE_URL_TEST - https://test.example.com
```

### c145 pubkeys:
```
repo.freistilbox.net ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIYUjsSuxX7uHJJrVrWwSIzcGG42O//Bg9qMS0eJf/CWbUG6//3R2sXU1vLdiNwgugZcayGvToyFxRmhpJ471tU9gOiNpOEVnn3EWSVTlVdN9QU3UYKuGVgSLt2irzOKOz/If8Wuv7ILOllpSqgKGrCp2jx9lY1uxu9qnSUY3iB1hZJMNag+U2kXh7s/9ICf5fyMtkTmBYreCCPyHG+iyMZuCJtYQITO/ChPcfEJEwxymjaIdtA/lTfDCNKgNhb7kiPjiUE+ofR2cI3mOUL7vAOaOr9UJCnUqmTQYe3jZvOSdhSPmDiAX9EBA58Iq9GFVt830D4ShlGSKp4t1dnXyj
c145s.freistilbox.net ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCoRKcrVrfR5cTeQlv5LS1LsC511IkR/iNJ479/k9dfNqSzzppa71HMNqueGWmH1a7yebc9ONNXIDmG9/q7rfO8iDxxYUpqtdHdKfVXhMPdtrFFpZG+kIfXNFIXe0mSStwxPQO8j3fy1VZMEBBg8mvPyrRT4XftkQT+P02db9HnvI5lXq0wqCD67cPPRLd9Zm96JCdlhsXUg18b2S756zQj8L41fHgAKzO6gumh1Zq6uwnYrfgb7ohZqbeXPd9zNhWPPHbDWeq/t3xhG0WcLsvZxtbfJ5P1gkhmYCMRLQq6KcjppDLcpad5eLoohqU4CDfF/GwzMBj8O1dVKc19To0D
```
(We need one for the deployment and one for the updb login)

## Update

To update scaffolded files, run the install script and use git to redo your customizations.

Also see:

* https://github.com/drupal-composer/drupal-project#should-i-commit-the-scaffolding-files

