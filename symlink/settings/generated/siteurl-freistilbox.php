<?php
$siteConfig = json_decode(file_get_contents(DRUPAL_ROOT . '/../config/site-config.json'), TRUE);
$protocol = is_file(DRUPAL_ROOT . '/../private/_envx.d/PROTO') ?
file_get_contents(DRUPAL_ROOT . '/../private/_envx.d/PROTO') : 'http';
$domain = $siteConfig['main_domain'];
$siteUrl = "$protocol://$domain";
return $siteUrl;
