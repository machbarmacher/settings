<?php
$isLive = getenv('WODBY_INSTANCE_TYPE') === 'prod';
$siteUrl = require DRUPAL_ROOT . '/../settings/generated/siteurl.php';
$instanceName = getenv('WODBY_INSTANCE_NAME');
// We include this here, rather than relying on wodby adding to s/d/settings.php.
include '/var/www/conf/wodby.settings.php';

// On wodby we are alone in our container, so 777 would be fine, we still use 775.
// If www-data writes files, wodby should own them too.
umask(002);
// Set sgid so dirs written by drush from wodby user inherit www-data group.
if ((fileperms('sites/default/files/') & 07777) !== 02775) {
  chmod('sites/default/files/', 02775);
}
// Set dir mask accordingly.
$settings['file_chmod_directory'] = 02775;
// This is standard.
$settings['file_chmod_file'] = 0664;

if (
  function_exists('posix_geteuid') // Linux only.
  && (
    !file_exists('sites/default/files/file-permissions-fixed-for-' . posix_getpwuid(posix_geteuid())['name'])
    || !(fileperms('sites/default/files/file-permissions-fixed-for-' . posix_getpwuid(posix_geteuid())['name']) & 0020) // g+w
  )
) {
  $it = new RecursiveDirectoryIterator('sites/default/files/',
    FilesystemIterator::KEY_AS_PATHNAME
    | FilesystemIterator::CURRENT_AS_FILEINFO
    | FilesystemIterator::SKIP_DOTS);
  /**
   * @var string $name
   * @var \SplFileInfo $file
   */
  foreach(new RecursiveIteratorIterator($it, RecursiveIteratorIterator::SELF_FIRST) as $name => $file) {
    @chmod($name, $file->isDir() ? 02775 : 0664);
  }
  file_put_contents('sites/default/files/file-permissions-fixed-for-' . posix_getpwuid(posix_geteuid())['name'], '');
}
