<?php

$siteConfig = json_decode(file_get_contents(DRUPAL_ROOT . '/../config/site-config.json'), TRUE);
$instanceName = $siteConfig['environment'];
$isLive = $siteConfig['environment'] === 'live';
$siteUrl = require DRUPAL_ROOT . '/../settings/generated/siteurl.php';

$conf['trusted_host_patterns'] = array_map(function ($domain) {
  return '^' . preg_quote($domain) . '$';
}, array_merge([$siteConfig['main_domain']], $siteConfig['alias_domains']));

require DRUPAL_ROOT . '/../config/drupal/settings-d7-site.php';
require glob(DRUPAL_ROOT . '/../config/drupal/settings-d7-db*.php')[0];
// This breaks the site without memcache module.
// require DRUPAL_ROOT . '/../config/drupal/settings-d7-memcache.php';

$conf['file_private_path'] = DRUPAL_ROOT . '/../private/default';
if (!file_exists(DRUPAL_ROOT . '/../private/default')) { mkdir(DRUPAL_ROOT . '/../private/default'); }

@include DRUPAL_ROOT . '/../private/_settings/settings.local.php';

// Redirect to HTTP(S) if necessary.
if (
  PHP_SAPI !== 'cli'
  && is_file(DRUPAL_ROOT . '/../private/_envx.d/PROTO')
  && ($protocolNeeded = trim(file_get_contents(DRUPAL_ROOT . '/../private/_envx.d/PROTO')))
  && ($protocolIs = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === "on" ? 'https' : 'http')
  && $protocolNeeded !== $protocolIs
) {
  $redirect = $protocolNeeded . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: ' . $redirect);
  exit();
}
